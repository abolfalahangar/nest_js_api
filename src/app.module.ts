import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrmConfig } from './ormconfig';

@Module({
  imports: [UsersModule, TypeOrmModule.forRoot(OrmConfig)],
  controllers: [],
  providers: [],
})
export class AppModule {}
