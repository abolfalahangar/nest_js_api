import { UserDomain } from 'src/users/domain/user.domain';

export interface IUserApplication {
  update(userDomain: UserDomain): Promise<UserDomain>;
  delete(id: string): Promise<UserDomain>;
  create(userDomain: UserDomain): Promise<UserDomain>;
  getById(id: string): Promise<UserDomain>;
}
