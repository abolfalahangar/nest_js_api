import { UserDomain } from 'src/users/domain/user.domain';

export interface IUserService {
  update(userDomain: UserDomain): Promise<UserDomain>;
  create(userDomain: UserDomain): Promise<UserDomain>;
  delete(id: string): Promise<UserDomain>;
  getById(id: string): Promise<UserDomain>;
}
