export const TYPES = {
  services: {
    IUserService: 'IUserService',
  },
  applications: {
    IUserApplication: 'IUserApplication',
  },
};
