import { IsString, IsEmail } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UserDomain {
  @IsString()
  @ApiProperty()
  readonly fullName: string;

  @IsString()
  @ApiProperty()
  readonly password: string;

  @IsEmail()
  @ApiProperty()
  readonly email: string;
}
