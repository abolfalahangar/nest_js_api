import { Module } from '@nestjs/common';
import { UsersController } from './controller/users.controller';
import { User } from './domain/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TYPES } from './interfaces/types';
import { UserApplication } from './applications/user.application';
import { UserService } from './services/user.service';

const userApp = {
  provide: TYPES.applications.IUserApplication,
  useClass: UserApplication,
};

const userService = {
  provide: TYPES.services.IUserService,
  useClass: UserService,
};

@Module({
  imports: [TypeOrmModule.forFeature([User])],
  controllers: [UsersController],
  providers: [userApp, userService],
})
export class UsersModule {}
