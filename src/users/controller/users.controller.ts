import {
  Controller,
  Inject,
  Post,
  Res,
  Body,
  HttpStatus,
  UsePipes,
  Get,
  Param,
  ParseUUIDPipe,
  Delete,
  Put,
} from '@nestjs/common';
import { UserDomain } from '../domain/user.domain';
import { TYPES } from '../interfaces/types';
import { ValidationPipe } from '../../common/validation.pipe';
import { IUserApplication } from '../interfaces/applications/user.application.interface';
import { ApiResponse, ApiTags, ApiParam } from '@nestjs/swagger';

@Controller('users')
@ApiTags('users')
export class UsersController {
  constructor(
    @Inject(TYPES.applications.IUserApplication)
    private userApp: IUserApplication,
  ) {}

  @UsePipes(new ValidationPipe())
  @Post()
  @ApiResponse({
    status: 201,
    description: 'The user has been successfully created.',
  })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  async create(
    @Res() res,
    @Body() userDomain: UserDomain,
  ): Promise<UserDomain> {
    const stock = await this.userApp.create(userDomain);
    return res.status(HttpStatus.OK).json(stock);
  }

  @Get(':id')
  @ApiParam({
    name: 'id',
    description: '',
    required: true,
  })
  @ApiResponse({
    status: 201,
    description: 'get user by id',
  })
  @ApiResponse({ status: 404, description: 'Not Found' })
  async findOne(@Param('id', new ParseUUIDPipe()) id): Promise<UserDomain> {
    const user = await this.userApp.getById(id);
    return user;
  }

  @ApiResponse({
    status: 200,
    description: 'The user has been successfully updated.',
  })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  @ApiResponse({ status: 404, description: 'Not Found' })
  @Put()
  async update(@Body() userDomain: UserDomain, @Res() res) {
    const user = await this.userApp.update(userDomain);

    res.status(HttpStatus.OK).json(user);
  }

  @ApiResponse({
    status: 204,
    description: 'The user has been successfully deleted.',
  })
  @ApiResponse({ status: 404, description: 'Not Found' })
  @Delete(':id')
  @ApiParam({
    name: 'id',
    description: '',
    required: true,
  })
  async delete(@Param('id', new ParseUUIDPipe()) id, @Res() res) {
    let user = await this.userApp.delete(id);
    return res.status(HttpStatus.NO_CONTENT).json(user);
  }
}
