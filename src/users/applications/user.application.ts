import { Injectable, Inject } from '@nestjs/common';
import { UserDomain } from '../domain/user.domain';
import { IUserApplication } from '../interfaces/applications/user.application.interface';
import { TYPES } from '../interfaces/types';
import { IUserService } from '../interfaces/services/user.service.interface';

@Injectable()
export class UserApplication implements IUserApplication {
  constructor(
    @Inject(TYPES.services.IUserService)
    private userService: IUserService,
  ) {}

  async update(user: UserDomain): Promise<UserDomain> {
    return await this.userService.update(user);
  }

  async getById(id: string): Promise<UserDomain> {
    const user = await this.userService.getById(id);

    return user;
  }

  async delete(id: string): Promise<UserDomain> {
    let user = await this.userService.delete(id);
    return user;
  }

  async create(user: UserDomain): Promise<UserDomain> {
    return await this.userService.create(user);
  }
}
