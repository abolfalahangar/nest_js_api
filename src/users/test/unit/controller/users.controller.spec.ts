import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from '../../../controller/users.controller';
import { TYPES } from '../../../interfaces/types';

const user = {
  userId: '123123123',
  fullName: 'Rafael Pezzetti',
  password: '123456',
  email: 'rafael@pezzetti.com',
};

class UserApplicationMock {
  getById(id) {
    return user;
  }
  create(obj) {
    return user;
  }
}

describe('Users Controller', () => {
  let controller: UsersController;
  let userAppMock;
  const response = {
    status: (code: number) => response,
    json: (json) => json,
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        {
          provide: TYPES.applications.IUserApplication,
          useClass: UserApplicationMock,
        },
      ],
    }).compile();

    controller = module.get<UsersController>(UsersController);
    userAppMock = module.get(TYPES.applications.IUserApplication);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  describe('findOne', () => {
    it('should get user by id', async () => {
      jest.spyOn(userAppMock, 'getById');

      expect(await controller.findOne(user.userId)).toEqual(user);
      expect(userAppMock.getById).toBeCalled();
    });
  });
  describe('create', () => {
    it('should create user', async () => {
      jest.spyOn(userAppMock, 'create');

      expect(await controller.create(response, user)).toEqual(user);
      expect(userAppMock.create).toBeCalled();
    });
  });
});
