import { Test } from '@nestjs/testing';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UserService } from '../../../services/user.service';
import { User } from '../../../domain/user.entity';

describe('CreateUserService', () => {
  let service: UserService;
  let repositoryMock: Repository<User>;
  beforeAll(async () => {
    const app = await Test.createTestingModule({
      providers: [
        UserService,
        {
          // how you provide the injection token in a test instance
          provide: getRepositoryToken(User),
          // as a class value, Repository needs no generics
          useClass: Repository,
        },
      ],
    }).compile();

    service = app.get<UserService>(UserService);
    repositoryMock = app.get<Repository<User>>(getRepositoryToken(User));
  });

  describe('create', () => {
    it('should create user', async () => {
      const userRepo: User = {
        userId: '79cd9f40-51b6-416e-8da0-9c09c66285f7',
        fullName: 'abolfazl',
        password: 'a19901990u',
        email: 'abolfazldeveloper12@gmail.com',
      };
      const user: User = {
        userId: '79cd9f40-51b6-416e-8da0-9c09c66285f7',
        fullName: 'abolfazl',
        password: 'a19901990u',
        email: 'abolfazldeveloper11@gmail.com',
      };
      jest.spyOn(repositoryMock, 'findOne').mockResolvedValueOnce(userRepo);
      jest.spyOn(repositoryMock, 'save').mockResolvedValueOnce(user);

      expect(await service.create(user)).toEqual(user);
      expect(repositoryMock.save).toBeCalled();
    });
  });
});
