import { Test } from '@nestjs/testing';
import { User } from '../../../domain/user.entity';
import { UserApplication } from '../../../applications/user.application';
import { TYPES } from '../../../interfaces/types';

class UserService {
  create(user) {
    return user;
  }
}
describe('CreateUserApplication', () => {
  let application: UserApplication;
  beforeAll(async () => {
    const app = await Test.createTestingModule({
      providers: [
        UserApplication,
        {
          provide: TYPES.services.IUserService,
          useClass: UserService,
        },
      ],
    }).compile();

    application = app.get<UserApplication>(UserApplication);
  });

  describe('create', () => {
    it('should create user', async () => {
      const user: User = {
        userId: '123123123',
        fullName: 'Rafael Pezzetti',
        password: '123456',
        email: 'rafael@pezzetti.com',
      };
      expect(await application.create(user)).toEqual(user);
    });
  });
});
