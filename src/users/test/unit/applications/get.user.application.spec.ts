import { Test } from '@nestjs/testing';
import { User } from '../../../domain/user.entity';
import { UserApplication } from '../../../applications/user.application';
import { TYPES } from '../../../interfaces/types';
import { NotFoundException } from '@nestjs/common';

const user: User = {
  userId: '123123123',
  fullName: 'Rafael Pezzetti',
  password: '123456',
  email: 'rafael@pezzetti.com',
};

class UserService {
  getById(userId) {
    return user;
  }
}

describe('GetUserApplication', () => {
  let application: UserApplication;
  let service: UserService;
  beforeAll(async () => {
    const app = await Test.createTestingModule({
      providers: [
        UserApplication,
        {
          provide: TYPES.services.IUserService,
          useClass: UserService,
        },
      ],
    }).compile();

    service = app.get<UserService>(TYPES.services.IUserService);
    application = app.get<UserApplication>(UserApplication);
  });

  describe('getById', () => {
    it('should get user by id', async () => {
      expect(await application.getById(user.userId)).toEqual(user);
    });

    it('throws 404 error when user is not found', async () => {
      jest.spyOn(service, 'getById').mockImplementation(() => null);
      try {
        await application.getById(user.userId);
      } catch (error) {
        expect(error).toBeInstanceOf(NotFoundException);
        expect(error.message.message).toEqual(
          `User with id ${user.userId} was not found`,
        );
      }
    });
  });
});
