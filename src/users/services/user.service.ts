import { IUserService } from 'src/users/interfaces/services/user.service.interface';
import { UserDomain } from '../domain/user.domain';
import { HttpStatus, HttpException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../domain/user.entity';
import { Repository } from 'typeorm';
export class UserService implements IUserService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
  ) {}
  async getById(id: string): Promise<UserDomain> {
    let user = await this.usersRepository.findOne({ userId: id });
    if (!user) {
      throw new NotFoundException(`User with id ${id} was not found`);
    }
    return user;
  }

  async delete(id: string): Promise<UserDomain> {
    let result = await this.usersRepository.findOne(id);
    if (!result) {
      throw new HttpException(
        {
          message: 'Invalid Payloadddd',
          errors: 'user not found',
          status: HttpStatus.NOT_FOUND,
        },
        HttpStatus.NOT_FOUND,
      );
    }

    await this.usersRepository.delete(result);
    return result;
  }

  async create(user: UserDomain): Promise<UserDomain> {
    let result = await this.usersRepository.findOne({ email: user.email });
    if (result?.email == user.email) {
      throw new HttpException(
        {
          message: 'Invalid Payload',
          errors: 'user aleready exist',
          status: HttpStatus.BAD_REQUEST,
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    return await this.usersRepository.save(user);
  }

  async update(user: UserDomain): Promise<UserDomain> {
    let result = await this.usersRepository.findOne({ email: user.email });
    if (!result) {
      throw new HttpException(
        {
          message: 'Invalid Payload',
          errors: 'user not found',
          status: HttpStatus.NOT_FOUND,
        },
        HttpStatus.NOT_FOUND,
      );
    }
    return await this.usersRepository.save(result);
  }
}
