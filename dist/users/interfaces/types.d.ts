export declare const TYPES: {
    services: {
        IUserService: string;
    };
    applications: {
        IUserApplication: string;
    };
};
