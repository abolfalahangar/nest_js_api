"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TYPES = void 0;
exports.TYPES = {
    services: {
        IUserService: 'IUserService',
    },
    applications: {
        IUserApplication: 'IUserApplication',
    },
};
//# sourceMappingURL=types.js.map