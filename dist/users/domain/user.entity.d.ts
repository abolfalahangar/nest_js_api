export declare class User {
    userId: string;
    fullName: string;
    email: string;
    password: string;
}
