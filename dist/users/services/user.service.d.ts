import { IUserService } from 'src/users/interfaces/services/user.service.interface';
import { UserDomain } from '../domain/user.domain';
import { User } from '../domain/user.entity';
import { Repository } from 'typeorm';
export declare class UserService implements IUserService {
    private usersRepository;
    constructor(usersRepository: Repository<User>);
    getById(id: string): Promise<UserDomain>;
    delete(id: string): Promise<UserDomain>;
    create(user: UserDomain): Promise<UserDomain>;
    update(user: UserDomain): Promise<UserDomain>;
}
