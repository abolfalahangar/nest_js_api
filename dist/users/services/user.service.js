"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const user_service_interface_1 = require("../interfaces/services/user.service.interface");
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const user_entity_1 = require("../domain/user.entity");
const typeorm_2 = require("typeorm");
let UserService = class UserService {
    constructor(usersRepository) {
        this.usersRepository = usersRepository;
    }
    async getById(id) {
        console.log('getById Run');
        let user = await this.usersRepository.findOne({ userId: id });
        console.log('user', user);
        if (!user) {
            throw new common_1.NotFoundException(`User with id ${id} was not found`);
        }
        return user;
    }
    async delete(id) {
        let result = await this.usersRepository.findOne(id);
        console.log('result', result);
        if (!result) {
            throw new common_1.HttpException({
                message: 'Invalid Payloadddd',
                errors: 'user not found',
                status: common_1.HttpStatus.NOT_FOUND,
            }, common_1.HttpStatus.NOT_FOUND);
        }
        await this.usersRepository.delete(result);
        return result;
    }
    async create(user) {
        let result = await this.usersRepository.findOne({ email: user.email });
        if ((result === null || result === void 0 ? void 0 : result.email) == user.email) {
            throw new common_1.HttpException({
                message: 'Invalid Payload',
                errors: 'user aleready exist',
                status: common_1.HttpStatus.BAD_REQUEST,
            }, common_1.HttpStatus.BAD_REQUEST);
        }
        return await this.usersRepository.save(user);
    }
    async update(user) {
        let result = await this.usersRepository.findOne({ email: user.email });
        if (!result) {
            throw new common_1.HttpException({
                message: 'Invalid Payload',
                errors: 'user not found',
                status: common_1.HttpStatus.NOT_FOUND,
            }, common_1.HttpStatus.NOT_FOUND);
        }
        return await this.usersRepository.save(result);
    }
};
UserService = __decorate([
    __param(0, (0, typeorm_1.InjectRepository)(user_entity_1.User)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map