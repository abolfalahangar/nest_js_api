"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserApplication = void 0;
const common_1 = require("@nestjs/common");
const types_1 = require("../interfaces/types");
let UserApplication = class UserApplication {
    constructor(userService) {
        this.userService = userService;
    }
    async update(user) {
        return await this.userService.update(user);
    }
    async getById(id) {
        const user = await this.userService.getById(id);
        return user;
    }
    async delete(id) {
        let user = await this.userService.delete(id);
        return user;
    }
    async create(user) {
        return await this.userService.create(user);
    }
};
UserApplication = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)(types_1.TYPES.services.IUserService)),
    __metadata("design:paramtypes", [Object])
], UserApplication);
exports.UserApplication = UserApplication;
//# sourceMappingURL=user.application.js.map