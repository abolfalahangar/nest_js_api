import { UserDomain } from '../domain/user.domain';
import { IUserApplication } from '../interfaces/applications/user.application.interface';
import { IUserService } from '../interfaces/services/user.service.interface';
export declare class UserApplication implements IUserApplication {
    private userService;
    constructor(userService: IUserService);
    update(user: UserDomain): Promise<UserDomain>;
    getById(id: string): Promise<UserDomain>;
    delete(id: string): Promise<UserDomain>;
    create(user: UserDomain): Promise<UserDomain>;
}
