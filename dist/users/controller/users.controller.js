"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersController = void 0;
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const user_domain_1 = require("../domain/user.domain");
const types_1 = require("../interfaces/types");
const validation_pipe_1 = require("../../common/validation.pipe");
const swagger_1 = require("@nestjs/swagger");
let UsersController = class UsersController {
    constructor(userApp) {
        this.userApp = userApp;
    }
    async create(res, userDomain) {
        const stock = await this.userApp.create(userDomain);
        return res.status(common_1.HttpStatus.OK).json(stock);
    }
    async findOne(id) {
        const user = await this.userApp.getById(id);
        return user;
    }
    async update(userDomain, res) {
        const user = await this.userApp.update(userDomain);
        res.status(common_1.HttpStatus.OK).json(user);
    }
    async delete(id, res) {
        let user = await this.userApp.delete(id);
        return res.status(common_1.HttpStatus.NO_CONTENT).json(user);
    }
};
__decorate([
    (0, common_1.UsePipes)(new validation_pipe_1.ValidationPipe()),
    (0, common_1.Post)(),
    (0, swagger_1.ApiResponse)({
        status: 201,
        description: 'The user has been successfully created.',
    }),
    (0, swagger_1.ApiResponse)({ status: 400, description: 'Bad Request' }),
    openapi.ApiResponse({ status: 201, type: require("../domain/user.domain").UserDomain }),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, user_domain_1.UserDomain]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "create", null);
__decorate([
    (0, common_1.Get)(':id'),
    (0, swagger_1.ApiParam)({
        name: 'id',
        description: '',
        required: true,
    }),
    (0, swagger_1.ApiResponse)({
        status: 201,
        description: 'get user by id',
    }),
    (0, swagger_1.ApiResponse)({ status: 404, description: 'Not Found' }),
    openapi.ApiResponse({ status: 200, type: require("../domain/user.domain").UserDomain }),
    __param(0, (0, common_1.Param)('id', new common_1.ParseUUIDPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "findOne", null);
__decorate([
    (0, swagger_1.ApiResponse)({
        status: 200,
        description: 'The user has been successfully updated.',
    }),
    (0, swagger_1.ApiResponse)({ status: 400, description: 'Bad Request' }),
    (0, swagger_1.ApiResponse)({ status: 404, description: 'Not Found' }),
    (0, common_1.Put)(),
    openapi.ApiResponse({ status: 200 }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_domain_1.UserDomain, Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "update", null);
__decorate([
    (0, swagger_1.ApiResponse)({
        status: 204,
        description: 'The user has been successfully deleted.',
    }),
    (0, swagger_1.ApiResponse)({ status: 404, description: 'Not Found' }),
    (0, common_1.Delete)(':id'),
    (0, swagger_1.ApiParam)({
        name: 'id',
        description: '',
        required: true,
    }),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, (0, common_1.Param)('id', new common_1.ParseUUIDPipe())),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "delete", null);
UsersController = __decorate([
    (0, common_1.Controller)('users'),
    (0, swagger_1.ApiTags)('users'),
    __param(0, (0, common_1.Inject)(types_1.TYPES.applications.IUserApplication)),
    __metadata("design:paramtypes", [Object])
], UsersController);
exports.UsersController = UsersController;
//# sourceMappingURL=users.controller.js.map