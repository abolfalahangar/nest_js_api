import { UserDomain } from '../domain/user.domain';
import { IUserApplication } from '../interfaces/applications/user.application.interface';
export declare class UsersController {
    private userApp;
    constructor(userApp: IUserApplication);
    create(res: any, userDomain: UserDomain): Promise<UserDomain>;
    findOne(id: any): Promise<UserDomain>;
    update(userDomain: UserDomain, res: any): Promise<void>;
    delete(id: any, res: any): Promise<any>;
}
