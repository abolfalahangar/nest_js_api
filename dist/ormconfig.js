"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrmConfig = void 0;
exports.OrmConfig = {
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'postgres',
    password: 'a19901990u',
    database: 'ddd',
    entities: [`${__dirname}/**/*.entity{.ts,.js}`],
    synchronize: true,
    migrations: ['migration/*.js'],
    cli: {
        entitiesDir: 'entity',
        migrationsDir: 'migration',
        subscribersDir: 'subscriber',
    },
};
//# sourceMappingURL=ormconfig.js.map