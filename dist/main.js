"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const validation_pipe_1 = require("./common/validation.pipe");
const swagger_1 = require("@nestjs/swagger");
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule, {});
    app.useGlobalPipes(new validation_pipe_1.ValidationPipe());
    const config = new swagger_1.DocumentBuilder()
        .setTitle('DDD example')
        .setDescription('The DDD API description')
        .setVersion('1.0')
        .addTag('DDD')
        .build();
    const document = swagger_1.SwaggerModule.createDocument(app, config);
    swagger_1.SwaggerModule.setup('api', app, document);
    await app.listen(3000);
    console.log('please open http://localhost:3000/api');
}
bootstrap();
//# sourceMappingURL=main.js.map